CREATE DATABASE  IF NOT EXISTS `metadata_gathering` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `metadata_gathering`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: metadata_gathering
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meta_categories`
--

DROP TABLE IF EXISTS `meta_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_categories` (
  `meta_cat_id` int NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(45) NOT NULL,
  PRIMARY KEY (`meta_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_categories`
--

LOCK TABLES `meta_categories` WRITE;
/*!40000 ALTER TABLE `meta_categories` DISABLE KEYS */;
INSERT INTO `meta_categories` VALUES (1,'System Introduction'),(2,'Data Generation or Creation'),(3,'Data Storage'),(4,'Data Usage'),(5,'Data Transformation'),(6,'Data Archiving'),(7,'Data Disposal');
/*!40000 ALTER TABLE `meta_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_choices`
--

DROP TABLE IF EXISTS `meta_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_choices` (
  `meta_choices_id` int NOT NULL AUTO_INCREMENT,
  `meta_question_id` int NOT NULL,
  `meta_choice_desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`meta_choices_id`),
  KEY `meta_ques_id_idx` (`meta_question_id`),
  CONSTRAINT `meta_ques_id` FOREIGN KEY (`meta_question_id`) REFERENCES `meta_questions` (`meta_question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1223 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_choices`
--

LOCK TABLES `meta_choices` WRITE;
/*!40000 ALTER TABLE `meta_choices` DISABLE KEYS */;
INSERT INTO `meta_choices` VALUES (101,1,'Indigenous'),(102,1,'Commercial Off the Shelf (COTS)'),(103,1,'Open Source'),(104,1,'Other (Please Specify)'),(105,2,'Cannot be Shared'),(106,2,'Human Readable Description of the System (Please Specify)'),(107,2,'Other (Please Specify)'),(108,3,'Year of Induction (Please Specify)'),(109,4,'Respective Original Equipment Manufacturer (OEM)'),(110,4,'Documented Manuals'),(111,4,'Subject Matter Expert (SME) (HR)'),(112,4,'Not Available'),(113,4,'Other (Please Specify)'),(114,5,'Structured (RDBMS, SQL, MySQL, ORACLE, etc.)'),(115,5,'Semi-Structured (System, Sensor or any Machine Generated Logs, etc.)'),(116,5,'Un-Structured (Media Data i.e. Image, Audio, Video, etc.)'),(117,6,'For a Limited Duration'),(118,6,'For Specific Situations or Excercises Only'),(119,6,'Continuous'),(120,6,'Other (Please Specify)'),(121,7,'Required to be Available for 24/7'),(122,7,'Affordable for Limited Duration (Please Specify the Duration)'),(123,7,'Other (Please Specify)'),(124,8,'Disposed'),(125,8,'Archived'),(126,8,'Will be Merged with the Data-Repository of some other System'),(127,8,'Any other Usage (Please Specify)'),(128,8,'Not Applicable (N/A)'),(129,9,'Structured'),(130,9,'Semi-Structured'),(131,9,'Un-Structured'),(132,10,'Structure of Data Describing Data Fields, Formats, Types, etc. (Please Specify)'),(133,11,'System will use its Complete Dataset'),(134,11,'System will use a Subset of its Dataset (Time Interval, Event Bases, etc.)'),(135,11,'Cannot be determined at this Stage'),(136,12,'NO'),(137,12,'YES (Please Specify Name of System)'),(138,12,'Cannot be determined at this Stage'),(139,13,'Secret'),(140,13,'Confidential'),(141,13,'Restricted'),(142,13,'Other (Please Specify)'),(143,14,'Secure Network (Intranet)'),(144,14,'Access Control Policy'),(145,14,'Encryption'),(146,14,'Other (Please Specify)'),(147,15,'Ensured by System Specific SOPs or Guidelines'),(148,15,'Enusred by Subject Matter Expert'),(149,15,'Ensured by System Owner OR Office of Primary Interest (OPI)'),(150,15,'Inbuilt Feature of System and Does not require any Human Intervention for Quality Control'),(151,15,'Other (Please Specify)'),(152,16,'Not Possible'),(153,16,'Through Alternate System (Please Specify Name of the System)'),(154,16,'Manual Support or Document Support'),(155,17,'Not Required'),(156,17,'Data Enty is Done by (Please Specify)'),(157,18,'Other (Please Specify)'),(158,19,'Periodically'),(159,19,'Not Required'),(160,20,'Humans'),(161,20,'Machines'),(162,20,'Does not Generate Data'),(163,20,'Other (Please Specify)'),(164,21,'24/7'),(165,21,'On Specific Condition (Time, Event, etc.)'),(166,21,'Other (Please Specify)'),(167,22,'Other (Please Specify) [in terms of Data Size i.e. (KB or MB or GB) / (sec or min or hour)]'),(168,23,'YES (Please Specify)'),(169,23,'NO'),(170,24,'Write Intensive'),(171,24,'Read Intensive'),(172,24,'Both'),(173,25,'Parent System'),(174,25,'Command Line'),(175,25,'Third Party Tools or Applications'),(176,25,'Other (Please Specify)'),(177,26,'Please Specify'),(178,27,'Limited with Parent Application'),(179,27,'Other (Please Specify)'),(180,28,'Defined '),(181,28,'Undefined'),(182,28,'Not Required at this Stage'),(183,28,'Not Recommended due to (Please Specify Reason)'),(184,29,'Please Specify'),(185,30,'YES'),(186,30,'NO'),(187,31,'Parent Application'),(188,31,'Can be Processed by Third Party Applications'),(189,31,'Would Require Specialist Skills for Detailed Analysis'),(190,31,'Cannot be determined at this Stage'),(191,32,'YES'),(192,32,'NO'),(193,33,'Scheduled (Please Specify the Scheduling Interval)'),(194,33,'As Per the Requirement'),(195,33,'Cannot be detetmined at this Time'),(196,33,'Not Required'),(197,34,'Other (Please Specify)'),(198,35,'Data is not Clean for Processing'),(199,35,'Data is Redundant'),(200,35,'Data can be Generated when and as Required (Data can be Reproduced)'),(201,35,'Data Storage Capacity Constraints'),(202,35,'Any Other (Please Specify)');
/*!40000 ALTER TABLE `meta_choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_forms`
--

DROP TABLE IF EXISTS `meta_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_forms` (
  `meta_form_id` int NOT NULL AUTO_INCREMENT,
  `meta_user_id` int NOT NULL,
  `created_at` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `meta_system_id` int NOT NULL,
  PRIMARY KEY (`meta_form_id`),
  KEY `meta_user_idx` (`meta_user_id`),
  KEY `meta_system_idx` (`meta_system_id`),
  CONSTRAINT `form_uid` FOREIGN KEY (`meta_user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `meta_system_id` FOREIGN KEY (`meta_system_id`) REFERENCES `meta_system` (`meta_system_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_forms`
--

LOCK TABLES `meta_forms` WRITE;
/*!40000 ALTER TABLE `meta_forms` DISABLE KEYS */;
INSERT INTO `meta_forms` VALUES (21,4,'2022-01-27 11:44:12','2022-02-02 09:39:59',2);
/*!40000 ALTER TABLE `meta_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_questions`
--

DROP TABLE IF EXISTS `meta_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_questions` (
  `meta_question_id` int NOT NULL AUTO_INCREMENT,
  `meta_cat_id` int NOT NULL,
  `meta_question` varchar(200) NOT NULL,
  `checkbox` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`meta_question_id`),
  KEY `meta_cat_id_idx` (`meta_cat_id`),
  CONSTRAINT `meta_cat_id` FOREIGN KEY (`meta_cat_id`) REFERENCES `meta_categories` (`meta_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_questions`
--

LOCK TABLES `meta_questions` WRITE;
/*!40000 ALTER TABLE `meta_questions` DISABLE KEYS */;
INSERT INTO `meta_questions` VALUES (1,1,'System Design',0),(2,1,'Short and Understandable Description of the System Feature',0),(3,1,'Year of Induction',0),(4,1,'If COTS, Internal details regarding Data Formats, Description is Available to',1),(5,1,'Data Storage Mechanism',1),(6,1,'System Life',0),(7,1,'System Downtime is',1),(8,1,'If Data is Generated for Limited Time, then Data Generated by the System would be',0),(9,1,'Nature of Data Generated by the System',1),(10,1,'Data Schema or Description (Metadata Information Only)',0),(11,1,'Data Usage by the System',0),(12,1,'Dependency on Other\'s System Data',0),(13,1,'Security Level of System and Data',0),(14,1,'Application or Data Security is Ensured through',1),(15,1,'Data Quality Control Practices',1),(16,1,'In Case of System Unavailability, alternate options to provide similar services',0),(17,1,'If Manually Supported, then after System Restoration, how manual Work or Data is imported in System',0),(18,1,'Additional Feature Requirement which may add further value to Utilization',0),(19,1,'System Utiliztion factor is Validated',0),(20,2,'System\'s Data is Generated by the',1),(21,2,'Data is Generated',0),(22,2,'Data Throughput (Volume of Data Production)',0),(23,2,'If Data Generation follows any Standard',0),(24,3,'Data Storage Requirement is',0),(25,3,'Data Access is possible through',1),(26,3,'Data Size or Volume',0),(27,4,'Data Usage is',0),(28,4,'Data Sharing Mechanism with other Applications',0),(29,4,'Any Standard, Governing the Data Sharing Requirement',0),(30,4,'Issues or Requirement of Data Interoperability',0),(31,5,'Data Processing and Transformation Requirement are done by the',1),(32,5,'Data Generated by System can be Transformed for Detailed or Customized Analysis',0),(33,6,'Frequency of Data Archiving',0),(34,6,'Any Special Consideration for Data Archiving',0),(35,7,'Requiement of Data-Disposal is due to',0);
/*!40000 ALTER TABLE `meta_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_responses`
--

DROP TABLE IF EXISTS `meta_responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_responses` (
  `meta_response_id` int NOT NULL AUTO_INCREMENT,
  `meta_form_id` int NOT NULL,
  `meta_user_id` int NOT NULL,
  `meta_question_id` int NOT NULL,
  `meta_answer_id` int NOT NULL,
  `meta_response_description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`meta_response_id`),
  KEY `meta_ques_id_idx` (`meta_question_id`),
  KEY `meta_u_id_idx` (`meta_user_id`),
  KEY `meta_ans_id_idx` (`meta_answer_id`),
  KEY `meta_form_idx` (`meta_form_id`),
  CONSTRAINT `meta_ans` FOREIGN KEY (`meta_answer_id`) REFERENCES `meta_choices` (`meta_choices_id`),
  CONSTRAINT `meta_form` FOREIGN KEY (`meta_form_id`) REFERENCES `meta_forms` (`meta_form_id`),
  CONSTRAINT `meta_ques` FOREIGN KEY (`meta_question_id`) REFERENCES `meta_questions` (`meta_question_id`),
  CONSTRAINT `meta_user` FOREIGN KEY (`meta_user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_responses`
--

LOCK TABLES `meta_responses` WRITE;
/*!40000 ALTER TABLE `meta_responses` DISABLE KEYS */;
INSERT INTO `meta_responses` VALUES (314,21,4,1,101,NULL),(315,21,4,2,106,'Data is generated through logs'),(316,21,4,3,108,'2018'),(317,21,4,4,109,NULL),(318,21,4,5,115,NULL),(319,21,4,6,118,NULL),(320,21,4,7,121,NULL),(321,21,4,8,125,NULL),(322,21,4,9,129,NULL),(323,21,4,10,132,'Confidential Information'),(324,21,4,11,133,NULL),(325,21,4,12,136,NULL),(326,21,4,13,139,NULL),(327,21,4,14,144,NULL),(328,21,4,15,148,NULL),(329,21,4,16,152,NULL),(330,21,4,17,155,NULL),(331,21,4,18,157,'Utilization Test Updated'),(332,21,4,19,159,NULL),(333,21,4,20,160,NULL),(334,21,4,21,165,NULL),(335,21,4,22,167,'1000GB Per Month'),(336,21,4,23,169,NULL),(337,21,4,24,170,NULL),(338,21,4,25,174,NULL),(339,21,4,26,177,'1TB'),(340,21,4,27,178,NULL),(341,21,4,28,181,NULL),(342,21,4,29,184,'Not Implemented'),(343,21,4,30,186,NULL),(344,21,4,31,188,NULL),(345,21,4,32,191,NULL),(346,21,4,33,193,'Once every week'),(347,21,4,34,197,'Testing'),(348,21,4,35,199,NULL);
/*!40000 ALTER TABLE `meta_responses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_system`
--

DROP TABLE IF EXISTS `meta_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_system` (
  `meta_system_id` int NOT NULL,
  `system_name` varchar(45) NOT NULL,
  PRIMARY KEY (`meta_system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_system`
--

LOCK TABLES `meta_system` WRITE;
/*!40000 ALTER TABLE `meta_system` DISABLE KEYS */;
INSERT INTO `meta_system` VALUES (1,'E-Courier'),(2,'Daak');
/*!40000 ALTER TABLE `meta_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'sohaib','sha256$irlVg3ZfxwDmgQMi$e055d332c7cba475a803eacaef3fb356fd73dcf8252f006b2b8beb4385f5ac80','2022-01-13 10:53:19'),(5,'rahat','sha256$exnDjvdLAhKKSSjq$9141814966cbcad16392bf62f1cb927f76d0a92f4aa3f73a788a9094616df25e','2022-01-18 14:28:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'metadata_gathering'
--

--
-- Dumping routines for database 'metadata_gathering'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-27  8:11:13
